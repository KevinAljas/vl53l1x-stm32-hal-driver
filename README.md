# VL53L1X Wrapper API
A wrapper API for the VL53L1X sensor ST drivers, written in C.

## Why?
The default API provided by ST for the VL53L1X is very cumbersome when dealing with multiple sensors on the same I2C peripheral. In addition, it does not handle the shutdown & interrupt pins.

## Goal
The goal of this API is simplifying the process of setting up & using VL53L1X sensor, at the cost of features, while still keeping as much portability as possible.

This wrapper API is designed to sit on top of the STM32 HAL & ST ToF driver. Managing multiple sensors is simplified by the use of C structures and helper functions, which handle more high-level logic.

A lot of the advanced features, like reading the "matrix distance map", while nice to have, are not needed for simple use cases, which is why the wrapper API doesn't currently implement them.

## Usage
The following instructions show how to set up the driver to be used in STM32 CubeIDE projects & how to use this driver in code.

Provided also is a example project for a STM32 Nucleo F303K8.

### Setting up for CubeIDE
1. Copy the VL53L1X folder into the CubeIDE project folder.
2. Add an include path to the VL53L1X and VL53L1X/Inc folders.
	This can be done by right-clicking the folders and clicking on "Add/remove include path..."
3. By default, CubeIDE generates driver files with names dependant on the target MCU. To avoid this, in the CubeIDE project, open the .ioc file and navigate to "Project Manager -> Code Generator". Tick the "Generate peripheral initialization as a pair of ".c/.h" files per peripheral".
4. Make sure you don't forget to configure I2C & XSHUT pins.
5. Sometimes, CubeIDE excludes copy-pasted files from the project. Right click on the VL53L1X folder, go to Properties -> "C/C++ Build" and untick "Exclude resource from build".

Now you should be able to include the main header file inside your project.

### Wrapper API functions
##### TOF_InitStruct()
```c
void TOF_InitStruct(VL53L1X* sensor, I2C_HandleTypeDef* hi2c, uint8_t address, GPIO_TypeDef* xshut_port, uint16_t xshut_pin);
```
This function is used to give initial values to the VL53L1X structure. That includes the I2C peripheral used, I2C address and the port and pin of XSHUT pin. This function doesn't actually communicate with the sensor and just configures values store in the MCU.

##### TOF_BootSensor()
```c
int TOF_BootSensor(VL53L1X* sensor);
```
This function configures a single sensors & makes it start measurements.

Note that this function will not work if you have multiple sensors on the same I2C line. It's recommended to use TOF_BootMultipleSensors() instead, so you don't need to manually turn all sensors on & off.

This function returns 0 on success, other values indicate failure. See logging to get more information when encountering errors.

##### TOF_BootMultipleSensors()
```c
int TOF_BootMultipleSensors(VL53L1X** sensors, uint8_t count);
```
This function configures multiple sensors & makes them start measurements.

It's recommended to use this function instead of multiple TOF_BootSensor() calls, because TOF_BootMultipleSensors() also takes care of managing the XSHUT pins.

This function returns 0 on success, other values indicate failure. See logging to get more information when encountering errors.

This function will try to configure all of the sensors, even if some previous initialization failed. The return value does not indicate how many sensors actually got initialized.

##### TOF_GetDistance()
```c
uint16_t TOF_GetDistance(const VL53L1X* sensor);
```
This function gets the most recent measurement from the sensor and returns it. The return value is an unsigned 16-bit integer, which shows the distance in millimeters.

Note, when this function returns 0xFFFF (or 65535), it indicates that reading the distance failed. Enable logging for more information.

##### TOF_GetDistance()
```c
void TOF_SetLogFunction(int (*prnt)(const char*, ...));
```
Calling this function with a corresponding function (like printf()) enables logging. 

To disable logging, just un-comment this function. To disable run-time logging, call this function with NULL.

More details can be seen in the following chapters.

##### Other functions
Other functions are for internal API usage only and are not covered in this README. For those interested, the code is not complex & should be easily readable.

### Using code for a single sensor
Let's look at an example of setting up a single sensor. All the API functions begin with TOF (which stands for Time of Flight).

```c
#include "VL53L1X.h"

int main(void) {
	// MCU peripheral setup, stuff like initializing clocks & I2C

	// Setup
	VL53L1X sensor1;
	TOF_InitStruct(&sensor1, &hi2c1, 0x32, XSHUT_GPIO_Port, XSHUT_GPIO_Pin);
	TOF_BootSensor(&sensor1);

	// Getting a measurement
	uint16_t distance = TOF_GetDistance(&sensor1);
	printf("%d\n\r", distance);
}
```

### Using code for multiple sensors
As mentioned before, the default ST driver makes it really annoying to setup multiple sensors.
The reason for this is that each sensor has the default address of 0x52. This is stored in volatile memory, which means that each time the sensor is rebooted or repowered, the address resets back to 0x52.
As a consequence, on a fresh power-up, with multiple sensors, each sensor has the same address & communication with them is impossible.

To avoid this problem, the VL53L1X sensors have a XSHUT pin. This XSHUT pin allows you to completely turn of the sensor. This allows you to configure multiple sensors by first, turning them all off & then turning them on one-by-one, while also giving each sensor a different address.

The ST ToF driver doesn't provide any functions to simplify this process and your main() function will include dozens of lines of setup code
for turning off every sensor and initializing them one by one. To make this process look more elegant in the main code, there is a function to initialize any amount of sensors at once.

```c
#include "VL53L1X.h"

int main(void) {
	// MCU peripheral setup, stuff like initializing clocks & I2C

	// Specify relevant parameters of each sensor
	VL53L1X sensor1, sensor2, sensor3;

	TOF_InitStruct(&sensor1, &hi2c1, 0x32, XSHUT1_GPIO_Port, XSHUT1_Pin);
	TOF_InitStruct(&sensor2, &hi2c1, 0x34, XSHUT2_GPIO_Port, XSHUT2_Pin);
	TOF_InitStruct(&sensor3, &hi2c1, 0x36, XSHUT3_GPIO_Port, XSHUT3_Pin);

	// Boot all sensors
	VL53L1X* sensors[] = {&sensor1, &sensor2, &sensor3};
	TOF_BootMultipleSensors(sensors, 3); // Array of sensors and how many sensors it contains

	// Getting measurements
	while(1) {
		// Read distances
		uint16_t d1 = TOF_GetDistance(&sensor1);
		uint16_t d2 = TOF_GetDistance(&sensor2);
		uint32_t d3 = TOF_GetDistance(&sensor3);

		// Do stuff with the measurements
		printf("%u, %u, %u", d1, d2, d3);
	}
}
```

### Note about I2C addresses
I2C addresses are (almost always) 7-bits. In the I2C protocol, this address gets an extra bit to signify if a read or write operation is about to happen.

Some manufacturers list I2C addresses as 8-bit read & write addresses. This is wrong (subjective opinion from the author). This driver expects that you provide a 7-bit address and it gets shifted in the back-end.
 
There are some limitations about which I2C addresses can be chosen.

1) The default address of the VL53L1X is 0x29. It's best practice to not use this address at all, although you technically can when using a single sensor or when using this address for the last sensor.

2) Some platforms use low & high I2C addresses for special meanings. As a rule of thumb, use addresses between 0x10 - 0x70, just to be safe.

3) Don't set duplicate addresses for sensors (e.g when you have two sensors, don't configure them both to have address 0x10). Technically this is acceptable when the sensors are on separate I2C peripherals but it's not recommended since logging uses I2C address as an identifier for which sensor failed.

4) If you have other devices on the I2C peripheral, take care to avoid conflicts.

Other than that, there are no limitations as to what address you can set it as. The example uses addresses 0x32, 0x34, 0x36 because it's a safe number in the middle of the I2C address space.

### Logging
While the driver is quite simple, things still can go wrong. To help with debugging, the API is capable of logging errors. 
To keep the API more platform independent, there is no built-in function which sends logs over UART or USB.
The way you provide this function is with TOF_SetLogFunction(), which takes as an argument a function pointer to a printf() compatible function.
```c
#include "VL53L1X.h"
#include "stdio.h"

int main(void) {
	// MCU peripheral setup, stuff like initializing clocks & I2C

	// Enable logging
	TOF_SetLogFunction(printf);

	VL53L1X sensor;
	TOF_InitStruct(&sensor, &hi2c1, 0x32, XSHUT1_GPIO_Port, XSHUT1_Pin);
	
	// Assume something is wrong with the sensor; either a missing wire or burned chip
	TOF_BootSensor(&sensor);

	// TOF_BootSensor() fails to initialize sensor and an error message is printed via the printf() function
	// "TOF Debug: Unable to configure address for sensor 0x32"
}
```

Important note: If you are copying this code to enable logging, make sure you have actually configured printf() to work. More information can be found [here](https://embeddingcpp.me/posts/003-stm32begin-002-printing/).

## Future improvements
Currently, a few things are not supported.  

1. FreeRTOS support; Would be nice to add functions similar to TOF_SetLogFunction() to allow the user to provide thread-safe alternatives to some functions. For example, currently the API uses HAL_Delay(), a fully blocking delay function.

2. Interrupts; VL53L1X have an extra pin which signals the MCU that the sensor has aquired a new reading. This allows the MCU to read from the sensor only when new data is present. However, this is not supported at the moment.

3. Document common issues, this driver is used by TalTech Robotics Club in the course about introductory robotics. It would be beneficial to compile a document of common issues that students encounter so they can solve them independently.

4. More complex operations, such as reading the distance "matrix".